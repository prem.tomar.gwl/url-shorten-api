let buildResponse = (status, body, url, message) => {
  return {
    status, body, url, message
  }
}

module.exports = {
  buildResponse
}