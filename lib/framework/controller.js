// var express = require('express');
let JaySchema = require("jayschema");
let jwt = require('jsonwebtoken');
const secret = require('../utils/config').secret;
let responseBuilder = require('../utils/response').buildResponse;
// var passport = require("passport");
// var authToken = require("./auth-token");
// var AuthUser = require("../auth/user");
var q = require('q');

function ResponseHandler(res) {
  return function (data) {
    console.log({ data })
    if (data.status === 302)
      res.redirect(data.url)
    else
      res.status(data.status).json(data.body);
  }
}



function ErrorHandler(next) {
  return function (err) {
    // var error = new Error(err.message || err);
    // error.status = err.status || 505;
    // error.message = err.message;
    // error.stack = err.body;
    next(err);
  }
}

function UserAgent(req, res, next) {
  console.log(req);
}


function Controller(actions, options) {

  if (typeof options === "undefined") {
    options = {};
  }



  var js = new JaySchema();

  js.addFormat("time2", function (time) {
    if (time.length === 5) {
      var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/
      if (timeRegex.test(time)) {
        return null;
      }
    }

    if (time.length === 8) {
      var timeRegex = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/
      if (timeRegex.test(time)) {
        return null;
      }
    }
    return "Invalid Time";
  });

  var loadParams = function (options, req, res, next) {
    var out = [];
    for (var i in req.params) {
      out.push(req.params[i]);
    }

    return out;
  };

  let checkToken = (req, res, next, data, options) => {
    let deferred = q.defer()
    if (options.auth) {
      let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase


      if (token) {
        if (token.startsWith('Bearer ')) {
          // Remove Bearer from string
          token = token.slice(7, token.length);
        }
        jwt.verify(token, secret, (err, decoded) => {
          if (err) {
            deferred.reject(responseBuilder(401, { error: "Invalid token" }, null, "Invalid token"))
          } else {
            req.decoded = decoded;
            deferred.resolve(data)
          }
        });
      } else {
        deferred.reject(responseBuilder(401, {error:"No Token provided"}, null, "No token provided"))
      }
    }
    else {
      deferred.resolve(data)
    }

    return deferred.promise
  };

  var validateSchema = function (options, req, data) {
    console.log(req.body)
    if (typeof options.schema !== "undefined") {
      var deferred = q.defer();
      var schema = require('../' + options.schema);
      var instance = req.body;

      js.validate(instance, schema, function (errs) {
        if (errs) {
          console.log(errs);
          return deferred.reject("Input validation: " + errs[0].desc);
        }

        data.push(instance);
        console.log(data)
        deferred.resolve(data);
      });



      return deferred.promise;
    }
    else {
      data.push(req.body);
    }

    return data;
  }

  function Action(action, options) {
    if (typeof options === "undefined") {
      options = {};
    }

    if (typeof action !== "function") {
      throw "Invalid Action: " + action;
    }
    var stack = [];





    var apiAction = function (req, res, next) {

      q(loadParams(options, req, res, next))
        .then((data) => {
          return checkToken(req, res, next, data, options)
        })
        .then(function (data) {
          return validateSchema(options, req, data);
        })
        .then(function (data) {
          return action.apply(this, data);
        })
        .then(ResponseHandler(res))
        .catch(ErrorHandler(next))
    };

    stack.push(apiAction);




    return stack;
  }

  var ControllerActions = {};
  for (var i in actions) {
    ControllerActions[i] = Action(actions[i], options[i])
  }



  return ControllerActions;
}

module.exports = Controller;
