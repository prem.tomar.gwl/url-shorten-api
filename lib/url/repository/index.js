var q = require('q')
var dbOp = require('../db')


let collapseUrl = (data) => {

  return dbOp.setKeyToUrl(data.url, data.customUrl);

}

/**
 * To get url from short url
 * @param {string} shortUrl 
 * @returns {Promise<string>}
 */

let expandUrl = (shortUrl) => {
  return dbOp.getOriginalUrl(shortUrl)
}



module.exports = {
  collapseUrl,
  expandUrl
}

