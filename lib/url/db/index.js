var db = require('../../framework/db').client;
var lua = require('../../framework/db').lua;
var q = require('q')
var responseBuilder = require('../../utils/response').buildResponse
var URL = require('url');



/**
 * 
 * @param {string} key short url string
 * @description To get original url from short url
 * @returns {Promise}
 */

let getOriginalUrl = (key) => {
  let deferred = q.defer()

  db.get(key, (err, val) => {
    if (err)
      deferred.reject(responseBuilder(400, err, null, null))
    else
      deferred.resolve(responseBuilder(302, null, val, null))
  })

  return deferred.promise
}

/**
 * @param {string} shortKey Optional custom short url string
 * @param {string }originalUrl url to shorten
 * @description to shorten given url
 * @returns {Promise}
 */
let setKeyToUrl = (originalUrl, shortKey) => {
  let deferred = q.defer()

  let key = shortKey || randomString(8, '#aA')

  db.set(key, originalUrl, function (err, val) {
    if (err)
      deferred.reject(responseBuilder(400, err, null, null))
    else {
      db.expire(key, 24 * 60 * 60 * 1000);
      deferred.resolve(responseBuilder(200, { url: URL.resolve("http://localhost:3000/api/v1/", key) }, null, null))
    }
  })

  return deferred.promise
}


function randomString(length, chars) {
  var mask = '';
  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';
  if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  var result = '';
  for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
  return result;
}


module.exports = {
  getOriginalUrl,
  setKeyToUrl
}