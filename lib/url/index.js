var q = require('q')
var urlRepo = require('./repository')

/**
 * @param {Object} data 
 */
let getShortUrl = (data) => {

  return urlRepo.collapseUrl(data)
}

let redirectUrl = (url) => {
  return urlRepo.expandUrl(url)
}

module.exports = {
  getShortUrl,
  redirectUrl
}
